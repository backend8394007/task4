const joinNumbersFromRange = (start, finish) =>{
    let index = start;
    let str = '';
    while (index !== finish + 1){
        str += index.toString();
        index += 1;
    }
    return str;
}
console.log(joinNumbersFromRange(5, 10));