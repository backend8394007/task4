def joinNumbersFromRange(start, finish):
    index = start
    result = ''
    while index != finish + 1:
        result += str(index)
        index += 1
    return result


print(joinNumbersFromRange(5, 10))
